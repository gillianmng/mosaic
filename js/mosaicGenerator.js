/**
 * Generates a mosaic of dots from an image file.
 */
class MosaicGenerator {

    constructor(tileWidth, tileHeight, canvasElem, mosaicResultElem) {
        this.tileWidth = tileWidth;
        this.tileHeight = tileHeight;
        this.canvasElem = canvasElem;
        this.mosaicResultElem = mosaicResultElem;
        this.MAX_IMAGE_WIDTH = 900;
        this.MAX_IMAGE_HEIGHT = 900;
    }

    /**
     * Get the svg element for a mosaic tile with a specific colour
     * @param colour
     * @returns {*}
     */
    static getMosaicTileElement(colour) {
        return $http('/color/' + colour)
            .get({})
            .then(function(res) {
                var parser = new DOMParser();
                return parser.parseFromString(res, "text/xml").firstChild;
            });
    }

    /**
     * Convert rgb colour to hexadecimal
     * @param r
     * @param g
     * @param b
     * @returns {string}
     */
    static convertRGBToHex(r, g, b) {
        return ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
    }

    /**
     * Get the average colour of an array of colours passed in
     * @param imageDataArray
     * @returns {string}
     */
    static getAverageColourHex(imageDataArray) {
        var totalR = 0, totalG = 0, totalB = 0;
        var numPixels = imageDataArray.length / 4;

        for(var i = 0; i < imageDataArray.length; i+=4) {
            totalR += imageDataArray[i];
            totalG += imageDataArray[i+1];
            totalB += imageDataArray[i+2];
        }

        return MosaicGenerator.convertRGBToHex(
            Math.round(totalR / numPixels),
            Math.round(totalG / numPixels),
            Math.round(totalB / numPixels));
    }

    /**
     * Create mosaic from an image
     * @param imageUrl
     * @returns {Promise}
     */
    createMosaic(imageUrl) {
        var self = this;

        var canvasContext = self.canvasElem.getContext('2d');
        var img = new Image();

        // clear the result element
        var last;
        while (last = self.mosaicResultElem.lastChild) {
            self.mosaicResultElem.removeChild(last);
        }

        // load the imageUrl into the canvas
        var imgLoadedPromise = new Promise(function(resolve, reject){
            img.addEventListener('load', function() {
                var canvasWidth = Math.min(img.width, self.MAX_IMAGE_WIDTH);
                var canvasHeight = Math.min(img.height, self.MAX_IMAGE_HEIGHT);

                self.canvasElem.width = canvasWidth;
                self.canvasElem.height = canvasHeight;

                // if the image is too large, then scale it down
                if (img.width > self.MAX_IMAGE_WIDTH || img.height > self.MAX_IMAGE_HEIGHT) {
                    canvasContext.drawImage(img, 0, 0,
                        img.width,
                        img.height,
                        0, 0,
                        self.MAX_IMAGE_WIDTH,
                        self.MAX_IMAGE_HEIGHT);
                } else {
                    canvasContext.drawImage(img, 0, 0);
                }

                var currX = 0;
                var currY = 0;

                var promiseCurrRow = 0;
                var promiseCurrCol = 0;
                var promises = [];

                // iterate through all the tiles in the image, each tile is of tileWidth x tileHeight
                while ((currX < canvasWidth) && (currY < canvasHeight)) {
                    if (currX == 0) {
                        promises[promiseCurrRow] = [];
                    }

                    // get the pixel colours in the tile.
                    // if the number of pixels in this tile is smaller than the specified tile size (e.g. at the edge of the image)
                    // then sample only the number of pixels to the edge
                    var widthRemaining = canvasWidth - currX;
                    var heightRemaining = canvasHeight - currY;

                    var imgData = canvasContext.getImageData(
                        currX, currY,
                        Math.min(self.tileWidth, widthRemaining),
                        Math.min(self.tileHeight, heightRemaining)).data;

                    // get the average colour of the tile
                    var avg = MosaicGenerator.getAverageColourHex(imgData);

                    // get the mosiac tile element from the server
                    promises[promiseCurrRow][promiseCurrCol] = MosaicGenerator.getMosaicTileElement(avg);

                    currX += self.tileWidth;
                    promiseCurrCol++;

                    // if at the end of a row
                    if (currX >= canvasWidth) {
                        var toResolve = promises[promiseCurrRow];

                        if (promiseCurrRow > 0) {
                            toResolve.concat(promises[promiseCurrRow-1]);
                        }

                        // draw the row when the whole row is completed
                        Promise.all(toResolve).then(function(rowRes) {
                            for(var index = 0; index < rowRes.length; index++) {
                                self.mosaicResultElem.appendChild(rowRes[index]);
                            }

                            // draw a line break
                            self.mosaicResultElem.appendChild(document.createElement('br'));
                        });

                        currX = 0;
                        currY += self.tileHeight;

                        promiseCurrCol = 0;
                        promiseCurrRow++;
                    }
                }

                resolve(img);
            });
        });

        img.src = imageUrl;

        return imgLoadedPromise;
    }
}