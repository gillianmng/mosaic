/**
 * Called when an image is loaded
 * @param event
 */
function loadImage(event) {
    var fileElem = document.getElementById('imgFile');
    var canvasElem = document.getElementById('imgCanvas');
    var mosaicResultElem = document.getElementById('mosaicResult');

    var imageUrl = window.URL.createObjectURL(fileElem.files[0]);
    var mosaicGenerator = new MosaicGenerator(TILE_WIDTH, TILE_HEIGHT, canvasElem, mosaicResultElem);
    mosaicGenerator.createMosaic(imageUrl).then(function(res){
        window.URL.revokeObjectURL(imageUrl);
    });
}

if (document.addEventListener) {
    document.addEventListener('DOMContentLoaded', function(event){
        document.getElementById('submit').addEventListener('click', loadImage, false);
    });
}